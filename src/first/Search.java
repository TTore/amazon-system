package first;

public interface Search {

    // TODO return List<Product>
    void searchProductsByName(String name);
    void searchProductsByCategory(String category);
}
